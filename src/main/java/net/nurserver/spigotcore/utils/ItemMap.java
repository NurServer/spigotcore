package net.nurserver.spigotcore.utils;

import com.google.common.collect.Maps;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ItemMap extends HashMap<String, InteractableItem>
{
    public void registerItem(String name, InteractableItem interactableItem)
    {
        this.put(name.toLowerCase(), interactableItem);
    }

    public InteractableItem getItem(String name)
    {
        return this.get(name.toLowerCase());
    }

    public InteractableItem getItem(ItemStack itemStack)
    {
        List<InteractableItem> items = this.values().stream().filter(i -> i.getItemStack().equals(itemStack))
                .collect(Collectors.toList());

        return items.size() > 0 ? items.get(0) : null;
    }
}
