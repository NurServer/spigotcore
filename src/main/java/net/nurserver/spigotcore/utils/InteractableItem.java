package net.nurserver.spigotcore.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@AllArgsConstructor @Data
public class InteractableItem
{
    private ItemStack itemStack;
    private Consumer<PlayerInteractEvent> interactHandler;
}
