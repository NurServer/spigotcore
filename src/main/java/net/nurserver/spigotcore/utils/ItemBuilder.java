package net.nurserver.spigotcore.utils;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	public static Builder builder(Material type, int amount) {
		ItemBuilder itemBuilder = new ItemBuilder();
		return itemBuilder.new Builder(type, amount);
	}

	public class Builder {

		private Material type;
		private int amount;
		private String displayName;
		private short damage;
		private List<String> lore;

		private Builder(Material type, int amount) {
			this.type = type;
			this.amount = amount;
			this.damage = 0;
		}

		public Builder amount(int amount) {
			this.amount = amount;
			return this;
		}

		public Builder type(Material type) {
			this.type = type;
			return this;
		}

		public Builder name(String name) {
			this.displayName = name;
			return this;
		}

		public Builder damage(short damage) {
			this.damage = damage;
			return this;
		}

		public Builder lore(String... lore) {
			this.lore = Arrays.asList(lore);
			return this;
		}

		public Builder lore(List<String> lore) {
			this.lore = lore;
			return this;
		}

		public ItemStack build() {
			ItemStack itemStack = new ItemStack(type, amount, damage);

			ItemMeta itemMeta = itemStack.getItemMeta();

			if (!(displayName == null || displayName.equals(""))) {
				itemMeta.setDisplayName(displayName);
			}

			if (lore != null && !lore.isEmpty()) {
				itemMeta.setLore(lore);
			}

			itemStack.setItemMeta(itemMeta);

			return itemStack;
		}
	}
}
