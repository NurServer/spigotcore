package net.nurserver.spigotcore.command;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import jdk.nashorn.internal.ir.annotations.Immutable;
import net.nurserver.Commons;
import net.nurserver.entities.Player;
import net.nurserver.entities.SimpleServer;
import net.nurserver.language.Language;
import net.nurserver.spigotcore.SpigotCore;
import net.nurserver.spigotcore.entities.SpigotServer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class SpigotCommand extends Command
{
    protected Player thePlayer;
    private boolean disabled;

    public SpigotCommand(String name)
    {
        super(name);
        this.setDescription(this.getName() + "Description");
        this.disabled = false;
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args)
    {
        if(sender instanceof org.bukkit.entity.Player)
        {
            thePlayer = Commons.getInstance().getPlayerManager().getPlayer(((org.bukkit.entity.Player) sender)
                    .getUniqueId());

            if(!thePlayer.hasPermissions(this.getPermission()))
                return true;
        }

        if(this.disabled)
        {
            if(thePlayer != null)
                thePlayer.sendLocalizedMessage("commandDisabled");
            else
                sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                        .getTranslation("commandDisabled"));
            return true;
        }

        this.process(sender, commandLabel, args);
        return true;
    }

    public abstract void process(CommandSender sender, String alias, String[] args);

    protected org.bukkit.entity.Player getAsBukkitPlayer(CommandSender sender)
    {
        return (org.bukkit.entity.Player) sender;
    }

    public void addAliases(String... strings)
    {
        this.getAliases().addAll(Arrays.asList(strings));
    }

    protected boolean playerProvided(CommandSender sender, int arg, String[] args)
    {
        if(args.length < arg)
        {
            return false;
        }

        return Commons.getInstance().getPlayerManager().getPlayer(args[arg]) != null;
    }

    public Language getLanguage()
    {
        if(this.thePlayer != null)
            return this.thePlayer.getLanguage();
        return Commons.getInstance().getLanguageManager().getDefaultLanguage();
    }

    @Override
    public String getDescription()
    {
        if(thePlayer != null)
            return this.thePlayer.getLanguage().getTranslation(this.description);
        else
           return Commons.getInstance().getLanguageManager().getDefaultLanguage().getTranslation(this.description);
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException
    {
        return ImmutableList.of();
    }

    protected List<String> getPlayerSuggestions(String arg)
    {
        return StringUtil.copyPartialMatches(arg, Commons.getInstance().getServer()
                .getPlayers()
                .stream()
                .map(Player::getName)
                .collect(Collectors.toList()),
                new ArrayList<>(Commons.getInstance().getServer()
                .getPlayers().size()));
    }

    protected SpigotServer getServer()
    {
        return (SpigotServer) Commons.getInstance().getServer();
    }
}
