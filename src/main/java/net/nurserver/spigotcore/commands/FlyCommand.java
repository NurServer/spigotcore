package net.nurserver.spigotcore.commands;

import net.nurserver.Commons;
import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand extends SpigotCommand
{
    public FlyCommand()
    {
        super("fly");
        this.setPermission("core.command.fly");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            Player player = this.getAsBukkitPlayer(sender);

            player.setAllowFlight(!player.getAllowFlight());
            player.sendMessage(thePlayer.getLanguage().getTranslation("flyToggled").replace("%STATE%",
                    player.getAllowFlight() ? thePlayer.getLanguage().getTranslation("enabled")
                            : thePlayer.getLanguage().getTranslation("disabled")));
        }
        else
        {
            sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                    .getTranslation("notAPlayer"));
        }
    }
}