package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

public class ChatClearCommand extends SpigotCommand
{
    public ChatClearCommand()
    {
        super("chatclear");
        this.setPermission("core.command.chatclear");
        this.addAliases("cc", "clearchat");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        String reason = null;

        if (args.length > 0)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 1; i < args.length; i++)
            {
                if (i != 1)
                    builder.append(" ");
                builder.append(args[i]);
            }

            reason = builder.toString();
        }

        this.getServer().clearChat(reason);
    }
}