package net.nurserver.spigotcore.commands;

import net.nurserver.Commons;
import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlySpeedCommand extends SpigotCommand
{
    public FlySpeedCommand()
    {
        super("flyspeed");
        this.setPermission("core.command.flyspeed");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            Player player = this.getAsBukkitPlayer(sender);

            if(args.length < 1)
            {
                player.sendMessage("§c/flyspeed <" + thePlayer.getLanguage().getTranslation("speed") + ">");
                return;
            }

            int flySpeed;

            try
            {
                flySpeed = Integer.parseInt(args[0]);
            }
            catch (Exception e)
            {
                player.sendMessage(thePlayer.getLanguage().getTranslation("argumentMustBeNumber")
                        .replace("%ARGUMENT%", "1"));
                return;
            }

            if(flySpeed > 10)
                flySpeed = 10;

            if(flySpeed < 0)
                flySpeed = 0;

            player.setFlySpeed(flySpeed / 10F);
            player.sendMessage(thePlayer.getLanguage().getTranslation("flySpeedChanged").replace("%SPEED%",
                    String.valueOf(player.getFlySpeed() * 10)));
        }
        else
        {
            sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                    .getTranslation("notAPlayer"));
        }
    }
}