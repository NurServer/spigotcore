package net.nurserver.spigotcore.commands;

import net.nurserver.entities.Player;
import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

public class ReloadCommand extends SpigotCommand
{
    public ReloadCommand()
    {
        super("reload");
        this.setPermission("core.command.reload");
        this.addAliases("rl");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        switch (args.length)
        {
            case 0:
            {
                for (Player player : this.getServer().getPlayers())
                    player.sendMessage(player.getLanguage().getTranslation("serverReloadStarted"));

                Bukkit.reload();

                for (Player player : this.getServer().getPlayers())
                    player.sendMessage(player.getLanguage().getTranslation("serverReloadFinished"));
                break;
            }
            case 1:
            {
                Plugin plugin;
                if((plugin = Bukkit.getPluginManager().getPlugin(args[0])) == null)
                {
                    sender.sendMessage(this.getLanguage().getTranslation("unknownPlugin")
                            .replace("%PLUGIN%", args[0]));
                    return;
                }

                Bukkit.getPluginManager().disablePlugin(plugin);
                Bukkit.getPluginManager().enablePlugin(plugin);

                sender.sendMessage(this.getLanguage().getTranslation("pluginReloaded")
                        .replace("%PLUGIN%", plugin.getName()));
                break;
            }
        }
    }
}
