package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.Commons;
import net.nurserver.utils.Formatter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class TpCommand extends SpigotCommand
{
    public TpCommand()
    {
        super("tp");
        this.setPermission("core.command.tp");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            if (args.length < 1)
            {
                sender.sendMessage(Formatter.help("tp", thePlayer.getLanguage()));
                return;
            }

            if (!this.playerProvided(sender, 0, args))
            {
                sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                        .getTranslation("playerNotFound"));
                return;
            }

            Player player = (Player) sender;
            Player target = Bukkit.getPlayer(args[0]);

            player.teleport(target.getLocation());
            player.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                    .getTranslation("teleportedSelfToOther").replaceAll("%TARGET%", target.getName()));
        }
        else
        {
            sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                    .getTranslation("notAPlayer"));
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException
    {
        if(args.length == 1)
        {
            return getPlayerSuggestions(args[0]);
        }
        return super.tabComplete(sender, alias, args);
    }
}
