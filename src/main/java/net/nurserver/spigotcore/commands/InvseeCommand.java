package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InvseeCommand extends SpigotCommand
{
    public InvseeCommand()
    {
        super("invsee");
        this.setPermission("core.command.invsee");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            Player player = this.getAsBukkitPlayer(sender);
            switch (args.length)
            {
                default:
                {
                    sender.sendMessage("§c/invsee <" + this.getLanguage().getTranslation("player") + ">");
                    break;
                }
                case 1:
                {
                    if (!playerProvided(sender, 0, args))
                    {
                        sender.sendMessage(this.getLanguage().getTranslation("playerNotFound"));
                        return;
                    }

                    player.openInventory(Bukkit.getPlayer(args[0]).getInventory());
                    break;
                }
            }
        }
        else
        {
            sender.sendMessage(this.getLanguage().getTranslation("notAPlayer"));
        }
    }
}
