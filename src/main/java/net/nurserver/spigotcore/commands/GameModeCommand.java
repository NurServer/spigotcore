package net.nurserver.spigotcore.commands;

import com.google.common.collect.ImmutableList;
import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.Commons;
import net.nurserver.entities.Player;
import net.nurserver.utils.Formatter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameModeCommand extends SpigotCommand
{
    public GameModeCommand()
    {
        super("gamemode");
        setAliases(ImmutableList.of("gm", "gms", "gmc", "gmsp", "gma"));
        this.setPermission("core.command.gamemode");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(args.length == 0)
        {
            if(thePlayer != null) sender.sendMessage(Formatter.help("gamemode", thePlayer.getLanguage()));
            else sender.sendMessage(Formatter.help("gamemode", Commons.getInstance().getLanguageManager().getDefaultLanguage()));
        }
        else if(args.length == 1)
        {
            if(sender instanceof org.bukkit.entity.Player)
            {
                Player player = Commons.getInstance().getPlayerManager().getPlayer(((org.bukkit.entity.Player) sender)
                        .getUniqueId());
                GameMode gameMode = getGameMode(args[0]);
                sender.sendMessage(player.getLanguage().getTranslation("gamemodeChangedSelf")
                        .replaceAll("%GAMEMODE%", gameMode.name()));
                getAsBukkitPlayer(sender).setGameMode(gameMode);
            }
            else
            {
                sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                        .getTranslation("notAPlayer"));
            }
        }
        else if(args.length == 2)
        {
            GameMode gameMode = getGameMode(args[0]);

            Player player = Commons.getInstance().getPlayerManager().getPlayer(((org.bukkit.entity.Player) sender)
                    .getUniqueId());

            if(!playerProvided(sender, 1, args))
            {
                player.sendMessage(player.getLanguage().getTranslation("playerNotFound"));
                return;
            }


            Player target = Commons.getInstance().getPlayerManager().getPlayer(args[1]);
            sender.sendMessage(player.getLanguage().getTranslation("gamemodeChangedOther")
                    .replaceAll("%GAMEMODE%", gameMode.name()).replaceAll("%TARGET%", target.getNameWithPrefix()));
            target.sendMessage(player.getLanguage().getTranslation("gamemodeChangedSelf")
                    .replaceAll("%GAMEMODE%", gameMode.name()));
            Bukkit.getPlayer(target.getUuid()).setGameMode(gameMode);
        }
    }

    private GameMode getGameMode(String arg)
    {
        GameMode gameMode = GameMode.SURVIVAL;
        if(arg.equalsIgnoreCase("survival") ||arg.equalsIgnoreCase("s") ||
               arg.equalsIgnoreCase("0"))
            gameMode = GameMode.SURVIVAL;
        else if(arg.equalsIgnoreCase("creative") ||arg.equalsIgnoreCase("c") ||
               arg.equalsIgnoreCase("1"))
            gameMode = GameMode.CREATIVE;
        else if(arg.equalsIgnoreCase("adventure") ||arg.equalsIgnoreCase("a") ||
               arg.equalsIgnoreCase("2"))
            gameMode = GameMode.ADVENTURE;
        else if(arg.equalsIgnoreCase("spectator") ||arg.equalsIgnoreCase("sp") ||
               arg.equalsIgnoreCase("3"))
            gameMode = GameMode.SPECTATOR;

        return gameMode;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException
    {
        if(args.length == 1)
        {
            return StringUtil.copyPartialMatches(args[1], Arrays.asList(Arrays.stream(GameMode.values())
                    .map(Enum::name).toArray(String[]::new)), new ArrayList<>(GameMode.values().length));
        }
        else if(args.length == 2)
        {
            return getPlayerSuggestions(args[1]);
        }
        return super.tabComplete(sender, alias, args);
    }
}
