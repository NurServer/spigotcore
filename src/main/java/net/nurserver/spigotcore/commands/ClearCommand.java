package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClearCommand extends SpigotCommand
{
    public ClearCommand()
    {
        super("clear");
        this.addAliases("cl");
        this.setPermission("core.command.clear");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(args.length == 0)
        {
            if(sender instanceof Player)
            {
                Player player = this.getAsBukkitPlayer(sender);

                player.getInventory().clear();
                player.getInventory().setArmorContents(null);
            }
            else
            {
                sender.sendMessage(this.getLanguage().getTranslation("notAPlayer"));
            }
            return;
        }

        if(!playerProvided(sender, 0, args))
        {
            sender.sendMessage(this.getLanguage().getTranslation("playerNotFound"));
            return;
        }

        Player target = Bukkit.getPlayer(args[0]);

        target.getInventory().clear();
        target.getInventory().setArmorContents(null);
    }
}
