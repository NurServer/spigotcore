package net.nurserver.spigotcore.commands;

import com.google.common.collect.ImmutableList;
import net.nurserver.Commons;
import net.nurserver.entities.Player;
import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.utils.Formatter;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class LanguageCommand extends SpigotCommand
{
    public LanguageCommand()
    {
        super("language");
        setAliases(ImmutableList.of("lang"));
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            if (args.length == 0)
            {
                sender.sendMessage(Formatter.help("language", thePlayer.getLanguage()));
                sender.sendMessage(Formatter.help("language list", thePlayer.getLanguage()));
            }
            else if (args.length == 1)
            {
                if (sender instanceof org.bukkit.entity.Player)
                {
                    Player player =
                            Commons.getInstance().getPlayerManager().getPlayer(((org.bukkit.entity.Player) sender)
                                    .getUniqueId());

                    if (args[0].equalsIgnoreCase("list"))
                    {
                        player.sendMessage(player.getLanguage().getTranslation("languageList")
                                .replaceAll("%LANGUAGES%", Arrays.toString(Commons.getInstance().getLanguageManager()
                                        .getLanguages().keySet().toArray()).toUpperCase()));
                        return;
                    }

                    if (Commons.getInstance().getLanguageManager().getLanguage(args[0].toLowerCase()) == null)
                    {
                        player.sendMessage(player.getLanguage().getTranslation("languageList")
                                .replaceAll("%LANGUAGES%", Arrays.toString(Commons.getInstance().getLanguageManager()
                                        .getLanguages().keySet().toArray()).toUpperCase()));
                        return;
                    }

                    player.setLanguage(args[0].toLowerCase());
                    player.sendMessage(player.getLanguage().getTranslation("languageChanged"));
                }
            }
        }
        else
        {
            sender.sendMessage(Commons.getInstance().getLanguageManager().getDefaultLanguage()
                    .getTranslation("notAPlayer"));
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException
    {
        if(args.length == 1)
        {
            Set<String> tabCompletes = Commons.getInstance().getLanguageManager()
                    .getLanguages().keySet();

            tabCompletes.add("list");

            return StringUtil.copyPartialMatches(args[0], tabCompletes,
                    new ArrayList<>(Commons.getInstance().getServer()
                            .getPlayers().size()));
        }
        return super.tabComplete(sender, alias, args);
    }
}
