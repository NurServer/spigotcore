package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GiveCommand extends SpigotCommand
{
    public GiveCommand()
    {
        super("give");
        this.addAliases("i", "item");
        this.setPermission("core.command.give");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        switch (args.length)
        {
            default:
            {
                sender.sendMessage("§c/give [" + this.getLanguage().getTranslation("player") + "] <" + this.getLanguage().getTranslation("item")
                        + "> <" + this.getLanguage().getTranslation("amount") + "> ");
                break;
            }
            case 2:
            {
                if(sender instanceof Player)
                {
                    Player player = this.getAsBukkitPlayer(sender);
                    Material material = Material.matchMaterial(args[0]);

                    if(material == null)
                    {
                        player.sendMessage(this.getLanguage().getTranslation("unknownMaterial"));
                        return;
                    }

                    int amount;

                    try
                    {
                        amount = Integer.parseInt(args[1]);
                    }
                    catch (Exception e)
                    {
                        player.sendMessage(this.getLanguage().getTranslation("argumentMustBeNumber")
                                .replace("%ARGUMENT%", "2"));
                        return;
                    }

                    if(amount < 1)
                        amount = 1;

                    player.getInventory().addItem(new ItemStack(material, amount));
                }
                else
                {
                    sender.sendMessage(this.getLanguage().getTranslation("notAPlayer"));
                }
                break;
            }
            case 3:
            {
                if(!playerProvided(sender, 0, args))
                {
                    sender.sendMessage(this.getLanguage().getTranslation("playerNotFound"));
                    return;
                }

                Player target = Bukkit.getPlayer(args[0]);
                Material material = Material.matchMaterial(args[1]);

                if(material == null)
                {
                    sender.sendMessage(this.getLanguage().getTranslation("unknownMaterial"));
                    return;
                }

                int amount;

                try
                {
                    amount = Integer.parseInt(args[2]);
                }
                catch (Exception e)
                {
                    sender.sendMessage(this.getLanguage().getTranslation("argumentMustBeNumber")
                            .replace("%ARGUMENT%", "3"));
                    return;
                }

                if(amount < 1)
                    amount = 1;

                target.getInventory().addItem(new ItemStack(material, amount));
                break;
            }
        }
    }
}
