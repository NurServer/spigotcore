package net.nurserver.spigotcore.commands;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.lib.player.OfflinePlayer;
import de.dytanic.cloudnet.lib.player.permission.GroupEntityData;
import net.nurserver.Commons;
import net.nurserver.entities.Player;
import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.utils.Formatter;
import org.bukkit.command.CommandSender;

public class SetGroupCommand extends SpigotCommand
{
    public SetGroupCommand()
    {
        super("setgroup");
        this.setPermission("core.command.setgroup");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(args.length < 2)
        {
            sender.sendMessage(Formatter.help("setgroup", thePlayer.getLanguage()));
        }
        else
        {
            Player player = Commons.getInstance().getPlayerManager().getPlayer(args[0]);
            if(player == null)
            {
                sender.sendMessage(this.getLanguage().getTranslation("playerNotFound"));
                return;
            }
            OfflinePlayer offlinePlayer = CloudAPI.getInstance().getOfflinePlayer(player.getUuid());

            if(CloudAPI.getInstance().getPermissionPool().getGroups().containsKey(args[1]))
            {
                player.sendMessage(player.getLanguage().getTranslation("permissionsGroupNotFound"));
                return;
            }

            offlinePlayer.getPermissionEntity().getGroups().clear();
            offlinePlayer.getPermissionEntity().getGroups().add(new GroupEntityData(args[4], 0L));

            player.sendMessage(player.getLanguage().getTranslation("permissionsGroupUpdated")
                    .replaceAll("%TARGET%", offlinePlayer.getName()).replaceAll("%GROUP%", args[1]));
        }
    }
}
