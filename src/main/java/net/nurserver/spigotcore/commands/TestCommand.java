package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.spigotcore.gui.Gui;
import net.nurserver.spigotcore.gui.GuiIcon;
import net.nurserver.spigotcore.gui.GuiSwitch;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class TestCommand extends SpigotCommand
{
    public TestCommand()
    {
        super("test");
        this.setPermission("core.command.test");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            Gui marcoGui = new Gui("Marco auch", 3);

            GuiIcon marcoGuiIcon = new GuiIcon(null, new ItemStack(Material.STONE), event -> {
                sender.sendMessage("You clicked the stone in the Marco GUI");
            }, marcoGui);

            marcoGui.addElement(0, new GuiSwitch(marcoGuiIcon, event ->
            {
            }, marcoGui,
                    new ItemStack(Material.DIRT), new ItemStack(Material.GRASS), true));

            marcoGui.addElement(1, marcoGuiIcon);

            Gui gui = new Gui("Frozen ist toll", 3);

            gui.setParent(marcoGui);

            GuiIcon guiIcon =
                    new GuiIcon(null, new ItemStack(Material.STONE), event -> marcoGui.open((Player) sender), gui);

            gui.addElement(0, new GuiSwitch(guiIcon, event ->
            {
            }, gui,
                    new ItemStack(Material.DIRT), new ItemStack(Material.GRASS), true));

            gui.addElement(1, guiIcon);

            gui.open((Player) sender);
        }
    }
}
