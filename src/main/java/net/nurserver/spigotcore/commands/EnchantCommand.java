package net.nurserver.spigotcore.commands;

import net.nurserver.spigotcore.SpigotCore;
import net.nurserver.spigotcore.command.SpigotCommand;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class EnchantCommand extends SpigotCommand
{
    public EnchantCommand()
    {
        super("enchant");
        this.setPermission("core.command.enchant");
    }

    @Override
    public void process(CommandSender sender, String alias, String[] args)
    {
        if(sender instanceof Player)
        {
            Player player = this.getAsBukkitPlayer(sender);

            switch (args.length)
            {
                default:
                {
                    player.sendMessage("§c/enchant <" + this.getLanguage().getTranslation("enchantment")
                            + "> <" + this.getLanguage().getTranslation("level") + "> ");
                    break;
                }

                case 2:
                {
                    Enchantment enchantment;

                    if((enchantment = Enchantment.getByKey(new NamespacedKey(SpigotCore.getInstance(), args[0]))) == null)
                    {
                        player.sendMessage(this.getLanguage().getTranslation("unknownEnchantment"));
                        return;
                    }

                    int level;

                    try
                    {
                        level = Integer.parseInt(args[1]);
                    }
                    catch (Exception e)
                    {
                        player.sendMessage(thePlayer.getLanguage().getTranslation("argumentMustBeNumber")
                                .replace("%ARGUMENT%", "2"));
                        return;
                    }

                    player.getInventory().getItemInMainHand().addUnsafeEnchantment(enchantment, level);
                    player.sendMessage(this.getLanguage().getTranslation("enchantmentApplied")
                            .replace("%ENCHANTMENT%", enchantment.getKey().getNamespace()
                                    .replace("%LEVEL%", String.valueOf(level))));

                    break;
                }
            }
        }
        else
        {
            sender.sendMessage(this.getLanguage().getTranslation("notAPlayer"));
        }
    }
}
