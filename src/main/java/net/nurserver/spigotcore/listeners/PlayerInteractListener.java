package net.nurserver.spigotcore.listeners;

import net.nurserver.spigotcore.SpigotCore;
import net.nurserver.spigotcore.utils.InteractableItem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener
{
    @EventHandler
    public void onInventoryClick(PlayerInteractEvent event)
    {
        InteractableItem item = SpigotCore.getInstance().getItemMap().getItem(event.getItem());

        if(item != null)
        {
            event.setCancelled(true);
            item.getInteractHandler().accept(event);
        }
    }
}
