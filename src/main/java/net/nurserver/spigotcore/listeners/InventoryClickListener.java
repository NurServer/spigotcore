package net.nurserver.spigotcore.listeners;

import net.nurserver.spigotcore.gui.Gui;
import net.nurserver.spigotcore.gui.GuiElement;
import net.nurserver.spigotcore.SpigotCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryClickListener implements Listener
{
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        if(event.getClickedInventory().getType() == InventoryType.CREATIVE ||
                event.getClickedInventory().getType() == InventoryType.PLAYER)
            return;

        if(!(event.getWhoClicked() instanceof Player))
            return;

        Gui gui = SpigotCore.getInstance().getGuiManager().getGuis().get(event.getWhoClicked().getUniqueId());

        if(gui == null)
            return;

        event.setCancelled(true);

        GuiElement guiElement = gui.getGuiElement(event.getSlot());

        if(guiElement != null)
        {
            guiElement.onClick(event);
        }
    }
}
