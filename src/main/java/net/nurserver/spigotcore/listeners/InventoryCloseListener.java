package net.nurserver.spigotcore.listeners;

import net.nurserver.spigotcore.SpigotCore;
import net.nurserver.spigotcore.gui.Gui;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;

public class InventoryCloseListener implements Listener
{
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event)
    {
        if(event.getInventory().getType() == InventoryType.CREATIVE ||
                event.getInventory().getType() == InventoryType.PLAYER)
            return;

        if(!(event.getPlayer() instanceof Player))
            return;
        Player player = (Player) event.getPlayer();

        Gui gui = SpigotCore.getInstance().getGuiManager().getGuis().get(player.getUniqueId());

        if(gui != null)
        {
            if(gui.getParent() != null)
            {
                Bukkit.getScheduler().runTaskLater(SpigotCore.getInstance(), () ->
                {
                    gui.getParent().open(player);
                    SpigotCore.getInstance().getGuiManager().getGuis().put(player.getUniqueId(), gui.getParent());
                }, 1L);
                return;
            }

            SpigotCore.getInstance().getGuiManager().getGuis().remove(player.getUniqueId());
        }
    }
}
