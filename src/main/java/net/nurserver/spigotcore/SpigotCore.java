package net.nurserver.spigotcore;

import de.dytanic.cloudnet.api.CloudAPI;
import net.nurserver.spigotcore.command.SpigotCommand;
import net.nurserver.spigotcore.entities.SpigotServer;
import net.nurserver.spigotcore.gui.GuiManager;
import net.nurserver.spigotcore.listeners.InventoryClickListener;
import net.nurserver.spigotcore.locations.LocationManager;
import net.nurserver.Commons;
import net.nurserver.spigotcore.listeners.InventoryCloseListener;
import lombok.Getter;
import net.nurserver.spigotcore.commands.*;
import net.nurserver.spigotcore.utils.ItemMap;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;

@Getter
public final class SpigotCore extends JavaPlugin
{
    @Getter
    private static SpigotCore instance;
    private LocationManager locationManager;
    private GuiManager guiManager;
    private ItemMap itemMap;

    @Override
    public void onEnable()
    {
        instance = this;
        new Commons(new SpigotServer(), this.getDataFolder());
        Commons.getInstance().getServer().setLogger(this.getLogger());
        Commons.getInstance().getServer().setName(CloudAPI.getInstance().getServerId());
        Commons.getInstance().init();
        this.locationManager = new LocationManager();
        this.guiManager = new GuiManager();
        this.itemMap = new ItemMap();

        this.registerCommand("core", new GameModeCommand());
        this.registerCommand("core", new TpCommand());
        this.registerCommand("core", new TpHereCommand());
        this.registerCommand("core", new LanguageCommand());
        this.registerCommand("core", new TestCommand());

        this.getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
        this.getServer().getPluginManager().registerEvents(new InventoryCloseListener(), this);
    }

    @Override
    public void onDisable()
    {
        this.locationManager.saveLocations();
    }

    public void registerCommand(String prefix, SpigotCommand command)
    {
        this.getCommandMap().register(prefix, command);
    }

    private CommandMap getCommandMap() {
        try {
            Field commandmap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            commandmap.setAccessible(true);
            return (CommandMap)commandmap.get(Bukkit.getServer());
        }
        catch (Throwable ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
