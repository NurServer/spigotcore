package net.nurserver.spigotcore.entities;

import com.google.common.collect.Sets;
import com.sun.istack.internal.Nullable;
import net.nurserver.Commons;
import net.nurserver.entities.Player;
import net.nurserver.entities.SimpleServer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SpigotServer extends SimpleServer
{
    @Override
    public void broadcast(String message)
    {
        Bukkit.broadcastMessage(message);
    }

    public void broadcast()
    {
        Bukkit.broadcastMessage("");
    }

    @Override
    public Set<Player> getPlayers()
    {
        List<Player> players = new ArrayList<>();

        for(org.bukkit.entity.Player player : Bukkit.getOnlinePlayers())
            players.add(Commons.getInstance().getPlayerManager().getPlayer(player.getUniqueId()));

        return Sets.newHashSet(players);
    }

    @Override
    public void sendMessage(Player player, String message)
    {
        Bukkit.getPlayer(player.getUuid()).sendMessage(message);
    }

    public void clearChat(@Nullable String reason)
    {
        for(int i = 0; i < 100; i++)
        {
            this.broadcast();
        }

        if(reason != null)
            this.getPlayers().forEach(p -> p.sendMessage(p.getLanguage().getTranslation("chatCleared")
                    .replace("%REASON%", reason)));
    }
}
