package net.nurserver.spigotcore.gui;

import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data
public class GuiIcon extends GuiElement
{
    public GuiIcon(GuiElement parent, ItemStack itemStack,
                   Consumer<InventoryClickEvent> method, Gui gui)
    {
        super(parent, itemStack, method, gui);
    }

    @Override
    public void onClick(InventoryClickEvent event)
    {
        this.getMethod().accept(event);
    }
}
