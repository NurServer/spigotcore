package net.nurserver.spigotcore.gui;

import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data
public class GuiSwitch extends GuiElementToggleable
{
    private Boolean defaultValue;
    private Boolean value;

    public GuiSwitch(GuiElement parent,
                     Consumer<InventoryClickEvent> method, Gui gui, ItemStack itemStackEnabled,
                     ItemStack itemStackDisabled, Boolean defaultValue)
    {
        super(parent, defaultValue ? itemStackEnabled : itemStackDisabled, method, gui, itemStackEnabled, itemStackDisabled);
        this.defaultValue = defaultValue;
        this.value = this.defaultValue;
    }

    @Override
    public void onClick(InventoryClickEvent event)
    {
        this.setValue(!this.getValue());
        this.setItemStack(this.getValue() ? this.getItemStackEnabled() : this.getItemStackDisabled());
        this.getGui().updateGuiElement(this);
        this.getMethod().accept(event);
    }
}
