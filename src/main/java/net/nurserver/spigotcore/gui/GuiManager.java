package net.nurserver.spigotcore.gui;

import com.google.common.collect.Maps;
import lombok.Data;

import java.util.TreeMap;
import java.util.UUID;

@Data
public final class GuiManager
{
    private TreeMap<UUID, Gui> guis;

    public GuiManager()
    {
        this.guis = Maps.newTreeMap();
    }
}
