package net.nurserver.spigotcore.gui;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data @AllArgsConstructor
public abstract class GuiElement
{
    private GuiElement parent;
    private ItemStack itemStack;
    private Consumer<InventoryClickEvent> method;
    private Gui gui;

    public abstract void onClick(InventoryClickEvent event);
}
