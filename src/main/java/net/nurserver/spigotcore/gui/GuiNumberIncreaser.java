package net.nurserver.spigotcore.gui;

import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data
public class GuiNumberIncreaser extends GuiElement
{
    public GuiNumberIncreaser(GuiNumber parent, ItemStack itemStack,
                              Consumer<InventoryClickEvent> method, Gui gui, int value, int min, int max)
    {
        super(parent, itemStack, method, gui);
    }

    @Override
    public void onClick(InventoryClickEvent event)
    {
        this.getParent();
        this.getMethod().accept(event);
    }
}
