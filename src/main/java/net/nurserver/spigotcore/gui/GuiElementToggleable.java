package net.nurserver.spigotcore.gui;

import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data
public abstract class GuiElementToggleable extends GuiElement
{
    private ItemStack itemStackEnabled;
    private ItemStack itemStackDisabled;

    public GuiElementToggleable(GuiElement parent, ItemStack itemStack,
                                Consumer<InventoryClickEvent> method, Gui gui,
                                ItemStack itemStackEnabled, ItemStack itemStackDisabled)
    {
        super(parent, itemStack, method, gui);
        this.itemStackEnabled = itemStackEnabled;
        this.itemStackDisabled = itemStackDisabled;
    }
}
