package net.nurserver.spigotcore.gui;

import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import net.nurserver.spigotcore.SpigotCore;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.TreeMap;

@Data
public final class Gui
{
    private TreeMap<Integer, GuiElement> guiElements;
    private Inventory inventory;
    private Gui parent;

    public Gui(String title, int rows)
    {
        this.guiElements = Maps.newTreeMap();
        this.inventory = Bukkit.createInventory(null,rows * 9, title);
    }

    public void open(Player player)
    {
        player.openInventory(this.inventory);
        SpigotCore.getInstance().getGuiManager().getGuis().put(player.getUniqueId(), this);
    }

    public void addElement(int slot, GuiElement guiElement)
    {
        this.getGuiElements().put(slot, guiElement);
        this.inventory.setItem(slot, guiElement.getItemStack());
    }

    public GuiElement getGuiElement(int slot)
    {
        return this.guiElements.get(slot);
    }

    public void updateGuiElement(GuiElement guiElement)
    {
        this.getGuiElements().forEach((k, v) -> {
            if(v.equals(guiElement))
            {
                this.inventory.setItem(k, guiElement.getItemStack());
            }
        });
    }

    @AllArgsConstructor @Data
    public static class Point
    {
        private int x, y;

        public int toSlot()
        {
            return y * 9 + x - 1;
        }
    }
}
