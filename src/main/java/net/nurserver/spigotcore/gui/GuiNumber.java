package net.nurserver.spigotcore.gui;

import lombok.Data;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

@Data
public class GuiNumber extends GuiElement
{
    private int value;
    private int min;
    private int max;

    public GuiNumber(GuiElement parent, ItemStack itemStack,
                     Consumer<InventoryClickEvent> method, Gui gui, int value, int min, int max)
    {
        super(parent, itemStack, method, gui);
        this.value = value;
        this.min = min;
        this.max = max;
    }

    @Override
    public void onClick(InventoryClickEvent event)
    {
        this.getMethod().accept(event);
    }
}
